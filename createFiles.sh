#!/bin/bash

lastFile=$(find $1 -type f -exec stat --format '%Y :%y %n' "{}" \; | sort -nr | cut -d: -f2- | head -n1 | grep ".gff" | wc -l)

if ["$lastFile" == "1" ]  # is the last modified file in this directory a gff file?
then
    perl gffToGTF.pl -exon all -GFF XENTR_9.1_Xenbase.gff
    ls -alh
else
    echo 'No need to run'
fi