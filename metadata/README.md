# Intro

This directory contains meta data for X. tropicalis genome.

# Scaffold names
* GCF_000004195.3_Xenopus_tropicalis_v9.1_assembly_report.txt
* Source: ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/004/195/GCF_000004195.3_Xenopus_tropicalis_v9.1/GCF_000004195.3_Xenopus_tropicalis_v9.1_assembly_report.txt


# Files from XenBase (needs to be documented)
* XENTR_9.1_Xenbase.geneInfo
* XENTR_9.1_Xenbase.overlap
