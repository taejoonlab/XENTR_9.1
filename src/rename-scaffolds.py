#!/usr/bin/env python3
import sys
import os

filename_gff = sys.argv[1]
target_db = 'UCSC'
if len(sys.argv) >= 3:
    target_db = sys.argv[2]

curr_dir = os.path.dirname(os.path.realpath(__file__))
filename_tbl = 'GCF_000004195.3_Xenopus_tropicalis_v9.1_assembly_report.txt'
filename_tbl = os.path.join(curr_dir, '..', 'metadata', filename_tbl)

xb_to_others = dict()
f_tbl = open(filename_tbl, 'r')
for line in f_tbl:
    if line.startswith('#'):
        continue
    tokens = line.strip().split("\t")
    xb_id = tokens[0]
    gb_id = tokens[4]
    refseq_id = tokens[6]
    ucsc_id = tokens[9]
    xb_to_others[xb_id] = {'Genbank': gb_id,
                           'RefSeq': refseq_id,
                           'UCSC': ucsc_id}
f_tbl.close()

f_gff = open(filename_gff, 'r')
for line in f_gff:
    if line.startswith('#'):
        print(line.strip())
        continue

    tokens = line.strip().split("\t")
    tmp_id = tokens[0]
    new_id = xb_to_others[tmp_id][target_db]
    print("%s\t%s" % (new_id, '\t'.join(tokens[1:])))
f_gff.close()
